package oop3;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Scanner;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

public class Interaction {
    
    public static void main (String[] args)
    {	Scanner scan = new Scanner(System.in);
        Interaction test = new Interaction();
        ArrayList<Fisch> fische = test.readXML("src/input.xml");
        int budget = 0;
        
        while (budget<=0){
            try{
                System.out.println("Buget eingeben (in Euro).");
                budget = Integer.parseInt(scan.next());
            }catch(Exception e){}
        }
        scan.close();
        
        Aquarium aqua = new Aquarium(fische,budget);
        ArrayList<Fisch> beschteListe = aqua.simulation();
        System.out.println("\nBeste Kombinationsmoeglichkeit\n");
        for (Fisch fish: beschteListe){
        	System.out.println(fish.getArt());
        }
        System.out.println("\nAlle besten Kombinationsmoeglichkeiten:\n");
        for (ArrayList<Fisch> fishs : aqua.getAllresults()){
            System.out.print("Kombinationsmoeglichkeit: ");
            for (Fisch fish: fishs){
        	System.out.print(fish.getArt()+"; ");
            }
            System.out.print("\n");
        }        
    }
    public Interaction(){
        
    }
    
    private ArrayList<Fisch> readXML(String path)
    {
        ArrayList<Fisch> result = new ArrayList<Fisch>();
        Document doc = null;
        try 
        {
            doc = parseXML(path);
        } 
        catch (ParserConfigurationException e) 
        {
            e.printStackTrace();
        } 
        catch (SAXException e) 
        {
            e.printStackTrace();
        } 
        catch (IOException e) 
        {
            e.printStackTrace();
        }

        if(doc != null)
        {
            NodeList xmlFische = doc.getElementsByTagName("fisch");
            for (int i = 0; i < xmlFische.getLength(); i++) 
            {
                //iterate throug all fisch nodes
                Node xmlFisch = xmlFische.item(i);
                NamedNodeMap attributes = xmlFisch.getAttributes();
                String id=attributes.getNamedItem("id").getNodeValue();
                String art=attributes.getNamedItem("art").getNodeValue();
                int preis=Integer.valueOf(attributes.getNamedItem("preis").getNodeValue());
                //create an Object Fisch with the values from the xml File
                Fisch fisch = new Fisch(id,art,preis);
                Element exmlFisch = (Element)xmlFisch;
                NodeList xmluFische = exmlFisch.getElementsByTagName("ufisch");
                for (int k = 0; k < xmluFische.getLength(); k++) 
                    {
                        //iterate through all ufisch nodes of the current fisch
                        Node xmluFisch = xmluFische.item(k);
                        attributes = xmluFisch.getAttributes();
                        id=attributes.getNamedItem("id").getNodeValue();
                        fisch.addUnvertaeglichkeit(id);
                    }
                result.add(fisch);
            }
        }
        return result;
    }

    private Document parseXML(String filePath) throws ParserConfigurationException, SAXException, IOException
    {
        DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
        DocumentBuilder db = dbf.newDocumentBuilder();
        Document doc = db.parse(filePath);
        doc.getDocumentElement().normalize();
        return doc;
    }
}

