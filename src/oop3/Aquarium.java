package oop3;

import java.util.ArrayList;
import java.util.TreeSet;

public class Aquarium {
	private ArrayList<Fisch> alleFische;		//enthaelt nur jeweils einen Fisch pro Art
	private ArrayList<Fisch> beschteListe; 	//kann einen Fisch mehrmals enthalten (falls buget uebrig ist)
	private int budget;
        private ArrayList<ArrayList<Fisch>> results;
        private  ArrayList<Fisch> result;
        
	public Aquarium(ArrayList<Fisch> alleFische, int budget){
		this.alleFische = alleFische;
                this.budget = budget;
		this.beschteListe = new ArrayList<Fisch>();
                ArrayList<ArrayList<Fisch>> results = new ArrayList<ArrayList<Fisch>>();
	}	
	
        public ArrayList<ArrayList<Fisch>> getAllresults(){
            ArrayList<ArrayList<Fisch>> outputs = new ArrayList<ArrayList<Fisch>>();
            for (ArrayList<Fisch> result : results){
                //dont output results which are shorter than chosen bestResult
                if (result.size()<this.result.size()) continue;
                //only ouput results which are not already outputted
                Boolean flag = true;
                for (ArrayList<Fisch> output : outputs){
                    Boolean flagInner = true;
                    for (Fisch fisch : result){
                        if (!output.contains(fisch)) flagInner = false;
                    }
                    if (flagInner) flag=false;
                }
                if (flag) outputs.add(result);
            }
            return outputs;
        }
        
        public ArrayList<Fisch> simulation (){
            return simulation( this.alleFische, this.budget);
	}
        
	public ArrayList<Fisch> simulation (ArrayList<Fisch> fische, int budget){
            this.results = new ArrayList<ArrayList<Fisch>>();
            beschteListe = rekursiv(fische,budget,new ArrayList<Fisch>());
            return beschteListe;
	}
      
	private ArrayList<Fisch> rekursiv (ArrayList<Fisch> restFische,int restBudget,ArrayList<Fisch> einkaufsFische){
            ArrayList<ArrayList<Fisch>> results = new ArrayList<ArrayList<Fisch>>();
            mainloop:
            for (Fisch fisch : restFische){
                //check all Constraints --> skip fisch if >= 1 contraint isnt met
                if (fisch.getKosten()>restBudget){
                    results.add(einkaufsFische);
                    continue mainloop;
                }
                for (Fisch testFisch : einkaufsFische) {
                    if (testFisch.isUnvertraeglich(fisch.getId())){
                        results.add(einkaufsFische);
                        continue mainloop;
                    }
                }
                //add fish to the list and continue the recursion
                //duplicate all values to avoid immutabillity errors
                int newRestBudget = restBudget - fisch.getKosten();
                ArrayList<Fisch> newEinkaufsFische = (ArrayList)(einkaufsFische.clone());
                newEinkaufsFische.add(fisch);
                ArrayList<Fisch> newRestFische = (ArrayList)(restFische.clone());
                newRestFische.remove(fisch);
                if (newRestFische.size()<=0){
                    return einkaufsFische;
                }
                results.add(rekursiv(newRestFische,newRestBudget,newEinkaufsFische));
            }
            this.results = results; //save all possible results
            this.result = bestResult(results); //alle möglichkeiten bearbeitet, return größte Liste
            return result;
	}
        
        private ArrayList<Fisch> bestResult(ArrayList<ArrayList<Fisch>> results){
            ArrayList<Fisch> bestResult = new ArrayList<Fisch>();
            if (results.isEmpty()) return bestResult;
            for (ArrayList<Fisch> result : results){
                if (bestResult.size()<result.size()) bestResult = result;
            }
            return bestResult;
        }
}
