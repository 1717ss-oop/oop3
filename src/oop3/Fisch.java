package oop3;

import java.util.ArrayList;

public class Fisch implements Comparable {

	private String art;
        private String id;
	private int kosten; 
	private ArrayList<String> unvertraeglich;
	      
	public Fisch(String id,String art,int kosten){
            this.id = id;
		this.art = art;
		this.kosten=kosten;
            this.unvertraeglich = new ArrayList<String>();
	}
	
	public void addUnvertaeglichkeit(String id){
		this.unvertraeglich.add(id);
	}
	
        public String getId(){
		return this.id;
	}
        
	public String getArt(){
		return this.art;
	}
	
	public int getKosten(){
		return this.kosten;
	}
	
	public ArrayList<String> getUnvertraeglich(){
		return this.unvertraeglich;
	}
        
        public Boolean isUnvertraeglich(Fisch fisch){
            return this.unvertraeglich.contains(fisch);
        }
        
        public Boolean isUnvertraeglich(String id){
            return this.unvertraeglich.contains(id);
        }
	
	public int compareTo(Object fisch){               
            return this.kosten - ((Fisch)fisch).getKosten();
	}
}
